﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackround : MonoBehaviour {

    private BoxCollider2D box;
    private float groundHorizontalLength;

	// Use this for initialization
	void Start () {
        box = GetComponent<BoxCollider2D>();
        groundHorizontalLength = box.size.x;
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x < -groundHorizontalLength) {
            RepositionBackground();
        }
	}

    void RepositionBackground() {
        Vector2 groundOffset = new Vector2(groundHorizontalLength * 2.0f, 0);
        transform.position = (Vector2)transform.position + groundOffset;
    }
}
