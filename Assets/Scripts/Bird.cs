﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {

    private float upForce = 200f;
    private bool isDead = false;
    private Rigidbody2D rigidBody;
    private Animator anim;

    // Use this for initialization
    void Start() {
        rigidBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!isDead) {

            #if UNITY_STANDALONE || UNITY_WEBPLAYER
            if (Input.GetMouseButtonDown(0))
            {
                rigidBody.velocity = Vector2.zero;
                rigidBody.AddForce(new Vector2(0, upForce));
                anim.SetTrigger("Flap");
            }
            #elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
            if (Input.touchCount > 0)
            {
                rigidBody.velocity = Vector2.zero;
                rigidBody.AddForce(new Vector2(0, upForce));
                anim.SetTrigger("Flap");
            }
            #endif
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isDead = true;
        anim.SetTrigger("Die");
        GameControl.instance.BirdDied();
    }
}
