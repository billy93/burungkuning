﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPool : MonoBehaviour {
    public float spawnTime = 4f;
    public GameObject column;

    public GameObject[] columns;
    public int columnPoolSize = 5;
    public int currentColumn = 0;
    public Vector2 objectPosition = new Vector2(-15f, -25f);

    public float timeSinceSpawned = 0;
    public float spawnXPosition = 10f;

    public float columnMin = -1f;
    public float columnMax = 3.5f;

    // Use this for initialization
    void Start () {
        columns = new GameObject[columnPoolSize];
        for(int i=0;i<columnPoolSize; i++)
        {
            columns[i] = Instantiate(column, objectPosition, Quaternion.identity);
        }
    }
	
	// Update is called once per frame
	void Update () {
        timeSinceSpawned += Time.deltaTime;

        if (GameControl.instance.gameover == false && timeSinceSpawned >= spawnTime) {
            timeSinceSpawned = 0;
            float spawnYPosition = Random.Range(columnMin, columnMax);
            columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentColumn++;

            if (currentColumn >= columnPoolSize) {
                currentColumn = 0;
            }
        }
    }
}
