﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public bool gameover = false;
    public GameObject gameOverText;
    public Text gameScoreText;

    public float scrollSpeed = -1.5f;

    public int score = 0;

    private void Awake()
    {
        if (instance == null) {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameover == true && Input.GetMouseButtonDown(0))
        {
#if UNITY_STANDALONE || UNITY_WEBPLAYER
            if (Input.GetMouseButtonDown(0)){
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
            if (Input.touchCount > 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
#endif
        }
    }

    public void BirdScored() {
        if (gameover == true) {
            return;
        }
        score++;
        gameScoreText.text = "Score : " + score.ToString();
    }

    public void BirdDied() {
        gameOverText.SetActive(true);
        gameover = true;
    }
}
